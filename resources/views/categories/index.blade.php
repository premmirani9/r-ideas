@extends('layouts.admin-panel.app')

@section('content')
<div class="d-flex justify-content-end mb-3">
    <a href="{{ route('categories.create') }}" class="btn btn-primary">Add Category</a>
</div>
<div class="card">
    <div class="card-header"><h2>Categories</h2></div>
    <div class="card-body">
        <table class="table">
            <thead>
              <tr>
                <th scope="col">Name</th>
                <th scope="col"> Actions</th>
              </tr>
            </thead>
            <tbody>
              @foreach ( $categories as $category)
              <tr>
                <td> {{ $category->name }} </td>
                <td>
                    <a href="{{ route('categories.edit',$category->id) }}" class="btn btn-warning">Edit</a>
                    <button type="button" onclick="displayModal({{$category->id}})" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">
                        Delete
                      </button>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
    </div>
  </div>
  <div class="d-flex justify-content-center mt-5">
      {{-- {{ $categories->links('vendor.pagination.bootstrap-4') }} --}}
  </div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Action Delete</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="" method="POST" id="deleteModalForm">
            @csrf
            @method('DELETE')
            <div class="modal-body">
                Are you sure, you want to delete the category ?
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger">Delete Category</button>
              </div>
        </form>
      </div>
    </div>
  </div>
@endsection
@section('page-level-scripts')
<script>
    function displayModal(categoryID){
        var url = "/categories/" + categoryID;
        $("#deleteModalForm").attr('action',url)
    }
</script>
@endsection
