@extends('layouts.admin-panel.app')

@section('content')

<div class="card">
    <div class="card-header">
        <h2>Edit Tag</h2>
    </div>
    <div class="card-body">
        <form action="{{ route('tags.update', $tag->id)}}" method="POST">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text"
                       class="form-control @error('name')
                           is-invalid
                       @enderror"
                       id="name"
                       name="name"
                       value="{{ $tag->name }}"
                       placeholder="Enter Tag Name:">
                @error('name')
                    <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                @enderror
            </div>
            <button type="submit"
                    name="submit"
                    class="btn btn-outline-success"
                    value="update">Update</button>
        </form>
    </div>
  </div>
@endsection
