<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use Illuminate\Http\Request;

class PostsController extends Controller
{

    public function index()
    {
        $posts = Post::paginate(2);
        return view('posts.index', compact('posts'));
    }

    public function create()
    {
        $categories = Category::all();
        $tags = Tag::all();
        return view('posts.create', compact(['categories','tags']));
    }

    public function store(CreatePostRequest $request)
    {
        //Image Upload and stores the name of the image
        $image = $request->file('image')->store('images/posts');
        // run command: php artisan storage:link
        //Create Post
        $post = Post::create([
            'title'=> $request->title,
            'excerpt'=> $request->excerpt,
            'content'=> $request->content,
            'image'=> $image,
            'published_at'=> $request->published_at,
            'user_id'=> auth()->id(),
            'category_id'=> $request->category_id
        ]);

        $post->tags()->attach($request->tags);
        session()->flash('success', 'Post Created Successfully!');
        return redirect(route('posts.index'));
    }

    public function show($id)
    {

    }

    public function edit(Post $post)
    {
        $categories = Category::all();
        $tags = Tag::all();
        return view('posts.edit', compact([
            'categories',
            'post',
            'tags'
        ]));
    }

    public function update(UpdatePostRequest $request, Post $post)
    {
        $data = $request->only(['title', 'excerpt', 'content', 'published_at', 'category_id']);
        if($request->hasFile('image')) {
            $image = $request->image->store('images/posts');
            $post->deleteImage();
            $data['image'] = $image;
        }
        $post->update($data);

        $post->tags()->sync($request->tags);

        session()->flash('success', 'Post updated successfully!');
        return redirect(route('posts.index'));
    }

    public function destroy($id)
    {

    }
}
