<?php

use App\Http\Controllers\FrontendController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [FrontendController::class, 'index'])->name('home');
Route::get('/blogs/{post}', [FrontendController::class, 'show'])->name('blogs.show');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');
require __DIR__.'/auth.php';


// Application routes
Route::resource('categories', App\Http\Controllers\CategoriesController::class)->middleware('auth');
Route::resource('tags', App\Http\Controllers\TagsController::class)->middleware('auth');
Route::resource('posts', App\Http\Controllers\PostsController::class)->middleware('auth');


