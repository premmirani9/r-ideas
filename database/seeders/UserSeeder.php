<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Himanshu Thakkar',
            'email' => 'himanshu@studylinkclasss.com',
            'password' => Hash::make('abcd1234')
        ]);

        User::create([
            'name' => 'Prem D Mirani',
            'email' => 'prem@studylinkclasss.com',
            'password' => Hash::make('abcd12343')
        ]);
    }
}
