<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;


class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categoryNews = Category::create(['name' => 'News']);
        $categorySports = Category::create(['name' => 'Sports']);
        $categoryScience = Category::create(['name' => 'Science']);
        $categoryStocks = Category::create(['name' => 'Stocks']);

        $tagCustomers = Tag::create(['name' => 'Customers']);
        $tagArt = Tag::create(['name' => 'Art']);
        $tagCrypto = Tag::create(['name' => 'Crypto']);
        $tagCricket = Tag::create(['name' => 'Cricket']);

        $post1 = Post::create([
            'title' => 'Covid 3rd wave for real?',
            'excerpt' => Factory::create()->sentence(rand(10,23)),
            'content' => Factory::create()->paragraphs(rand(10,23),true),
            'image' => 'images/posts/1.jpg',
            'category_id' => $categoryNews->id,
            'user_id' => 2,
            'published_at' => Carbon::now()->format('Y-m-d')
        ]);

        $post2 = Post::create([
            'title' => 'New zealand won world test championship',
            'excerpt' => Factory::create()->sentence(rand(10,23)),
            'content' => Factory::create()->paragraphs(rand(10,23),true),
            'image' => 'images/posts/2.jpg',
            'category_id' => $categorySports->id,
            'user_id' => 1,
            'published_at' => Carbon::now()->format('Y-m-d')
        ]);

        $post3 = Post::create([
            'title' => 'Earth flat?',
            'excerpt' => Factory::create()->sentence(rand(10,23)),
            'content' => Factory::create()->paragraphs(rand(10,23),true),
            'image' => 'images/posts/3.jpg',
            'category_id' => $categoryScience->id,
            'user_id' => 2,
            'published_at' => Carbon::now()->format('Y-m-d')
        ]);

        $post4 = Post::create([
            'title' => 'HFCL will hit 100 by december end',
            'excerpt' => Factory::create()->sentence(rand(10,23)),
            'content' => Factory::create()->paragraphs(rand(10,23),true),
            'image' => 'images/posts/1.jpg',
            'category_id' => $categoryStocks->id,
            'user_id' => 1,
            'published_at' => Carbon::now()->format('Y-m-d')
        ]);

        $post1->tags()->attach([$tagArt->id]);
        $post2->tags()->attach([$tagArt->id, $tagCricket->id]);
        $post3->tags()->attach([$tagArt->id, $tagCrypto->id]);
        $post4->tags()->attach([$tagArt->id]);
    }

}
